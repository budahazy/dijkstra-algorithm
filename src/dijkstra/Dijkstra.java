package dijkstra;

import java.util.LinkedList;
import java.util.List;

public class Dijkstra {

    public static List<String> searchPath(final String start, final String end) {
        List<Node> list = NodeFactory.getAllNodes();
        Node endNode = NodeFactory.getExistingNode(end);
        List<String> retList = new LinkedList<>();
        Node n = NodeFactory.getExistingNode(start);
        n.dist = 0;
        while (!list.isEmpty()) {
            Node u = Dijkstra.getSmallestNode(list);
            list.remove(u);
            if (u == endNode) {
                Node current = u;
                while (current.prev != null) {
                    retList.add(0, current.name);
                    current = current.prev;
                }
                return retList;
            }
            for (Node.Pair v : u.neigs) {
                if (list.contains(v.node)) {
                    int a = u.dist + v.cost;
                    if (a < v.node.dist) {
                        v.node.dist = a;
                        v.node.prev = u;
                    }
                }

            }

        }
        return new LinkedList();
    }

    public static Node getSmallestNode(List<Node> list) {
        return list.stream().min((n1, n2) -> {
            return Integer.compare(n1.dist, n2.dist);
        }).get();

    }
}
