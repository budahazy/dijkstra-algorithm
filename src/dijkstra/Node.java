package dijkstra;

import java.util.LinkedList;
import java.util.List;

public class Node {

    class Pair {

        int cost;
        Node node;

        public Pair(int cost, Node node) {
            this.cost = cost;
            this.node = node;
        }

        public int getCost() {
            return cost;
        }

    }

    final String name;
    int dist;
    Node prev;
    final List<Pair> neigs;

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public String getName() {
        return name;
    }

    public void addNeigs(int t, Node n) {
        Pair p = new Pair(t, n);
        this.neigs.add(p);
    }

    public List<Pair> getAllNegis() {
        return this.neigs;
    }

    public Node(String name) {
        this.name = name;
        dist = 500000;
        prev = null;
        neigs = new LinkedList<>();
    }

}
