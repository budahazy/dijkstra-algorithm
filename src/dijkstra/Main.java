package dijkstra;

import dijkstra.Node.Pair;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Button btn_openFile = new Button();

        btn_openFile.setText("Fájl megnyitása...");
        btn_openFile.setOnAction((ActionEvent event) -> {
            System.out.println("Fájl megnyitása megnyomva");
            FileChooser fc = new FileChooser();
            File f = fc.showOpenDialog(primaryStage);
            try {
                Scanner sc = new Scanner(f);
                while (sc.hasNext()) {
                    String varos1 = sc.next();
                    String varos2 = sc.next();
                    int tavolsag = sc.nextInt();
                    Node node = NodeFactory.getNode(varos1);
                    node.addNeigs(tavolsag, NodeFactory.getNode(varos2));
                }
            } catch (FileNotFoundException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(ex.getMessage());
                alert.showAndWait();
            }
        });
        TextField tf_from = new TextField();
        TextField tf_to = new TextField();
        TextField tf_output = new TextField();

        Button btn_findPath = new Button();
        btn_findPath.setText("Legrövidebb út keresése");
        btn_findPath.setOnAction((ActionEvent event) -> {
            System.out.println("Legrövidebb út megnyomva");
            List<String> ered = Dijkstra.searchPath(tf_from.getText(), tf_to.getText());
            System.out.println(ered.size());
            for (String string : ered) {
                tf_output.setText(tf_output.getText().concat(string + " -> "));
            }

        });

        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        StackPane root = new StackPane();
        root.getChildren().add(vbox);
        vbox.getChildren().add(btn_openFile);
        vbox.getChildren().add(tf_from);
        vbox.getChildren().add(tf_to);
        vbox.getChildren().add(btn_findPath);
        vbox.getChildren().add(tf_output);
        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Dijkstra");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
