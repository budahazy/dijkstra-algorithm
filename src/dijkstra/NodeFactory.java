package dijkstra;

import java.util.LinkedList;
import java.util.List;

public class NodeFactory {

    static LinkedList<Node> nodes = new LinkedList<>();

    public static Node getNode(final String city) {
        for (final Node node : nodes) {
            if (node.getName().equals(city)) {
                return node;
            }
        }

        Node newNode = new Node(city);
        nodes.add(newNode);
        return newNode;
    }

    public static Node getExistingNode(final String city) {
        for (final Node node : nodes) {
            if (node.getName().equals(city)) {
                return node;
            }
        }
        return null;
    }

    public static List<Node> getAllNodes() {
        return (LinkedList<Node>) nodes.clone();
    }

}
